* NAIVE

** Why NAIVE

We have a couple of packages with */-naive-/* in the name. What
follows is what we[fn:1] try to keep in mind when designing and building
such a package.

/Minimal, extensible, hackable, single purpose package that does its
job *WELL ENOUGH*./

*** Minimal

We try to keep a package simple and short. That usually translates to
a minimal basic usage API[fn:2].

*** Extensible

You can use it out of the box, but it tries to predict your wish list
and lets you add to or tweak[fn:3] where practical.

*** Hackable

No rocket science code. Code should be easy to understand by entry
level lisp programmer.

*** Single Purpose

Do one job, it is however not that simple for jobs with a meatier
complex. And some times a package just outgrows its shoes ;)

*** WELL ENOUGH

80/20 principal[fn:4], with good enough performance for the average project.

** Epilogue

We dont always reach all of these lofty goals, but hey we believe in
the 80/20 principal so its ok.

[fn:1] Who are /we/? Well that is me and the little voices in my head
:P.  Ok, ok and some times other significant contributors that where
begged, bullied or wooed to help ;)

[fn:2] And sometimes a customization API that causes considerable
bloat but we try to keep them apart in documentation where possible.

[fn:3] Interesting enough its the little tweaks that quickly bloats and API.

[fn:4] Some times a bit more 60/40 but with good intentions.
